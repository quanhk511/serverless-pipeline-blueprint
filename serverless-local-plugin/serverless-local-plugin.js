'use strict';
const promise = require('bluebird');
const YAML = require('yaml');

class serverlessLocalPlugin {
    constructor(serverless, options) {
        this.serverless = serverless;
        this.options = options;
        this.hooks = {
            'aws:package:finalize:mergeCustomProviderResources': this.finalize.bind(this)
        };

        this.overrideServerlessConfig()

    }

    overrideServerlessConfig() {
        const currentCustom = this.serverless.configurationInput.custom;
        const overrideCustom = Object.assign(currentCustom, {
            "onlyS3CustomResource": false,
            "disableCloudWatchLog": false
        });
        this.serverless.extendConfiguration(['custom'], overrideCustom);
        this.serverless.extendConfiguration(['provider', 'stage'], 'v1');
        this.serverless.extendConfiguration(['provider', 'deploymentBucket'], {
            "name": {
                "Ref": "DeploymentBucket"
            }
        });
        console.log(YAML.stringify(this.serverless.configurationInput))
    }
    finalize() {
        return promise.bind(this)
            .then(() => this.modifyParamters())
            .then(() => this.modifyResourceLambdas())
            .then(() => this.modifyOutputs())
            .then(() => this.modifyResourceS3Customs())
            .then(() => this.modifyResourceLogGroups());
    }
    modifyParamters() {
        const template = this.serverless.service.provider.compiledCloudFormationTemplate;
        const parameters = template['Parameters'];
        parameters['Environment'] = {
            "Type": "String"
        };
        parameters['DeploymentBucket'] = {
            "Type": "String"
        }
    }
    modifyResourceLogGroups() {
        const template = this.serverless.service.provider.compiledCloudFormationTemplate;
        const {
            custom
        } = this.serverless.configurationInput;
        const resources = template['Resources'];
        if (custom && 'disableCloudWatchLog' in custom && custom['disableCloudWatchLog']) {
            const deleted_cw_log = [];
            for (const item in resources) {
                if (resources[item]['Type'] == 'AWS::Logs::LogGroup') {
                    deleted_cw_log.push(item);
                }
            }
            for (const index in deleted_cw_log) {
                delete resources[deleted_cw_log[index]];
            }
            if ('IamRoleLambdaExecution' in resources) {
                var statements = resources['IamRoleLambdaExecution']['Properties']['Policies'][0]['PolicyDocument']['Statement'];
                const deleted_statements = [];
                for (const index in statements) {
                    var first_action = statements[index]['Action'][0];
                    if (first_action.startsWith('logs')) {
                        deleted_statements.push(index);
                    }
                }
                for (const index in deleted_statements.reverse()) {
                    statements.splice(deleted_statements[index], 1);
                }
            }
        }
        else {
            const parameters = template['Parameters'];
            parameters['LogRetentionInDays'] = {
                "Type": "String",
                "Default": 30,
                "AllowedValues": [
                    3, 5, 7, 14, 30
                ]
            }
            for (const resource in resources) {
                if (resources[resource].Type === 'AWS::Logs::LogGroup') {
                    const properties = resources[resource]['Properties'];
                    let logGroupName = properties.LogGroupName;
                    properties.LogGroupName = {
                        "Fn::Sub": `${logGroupName}-\${Environment}`
                    }
                    template.Resources[resource].Properties.RetentionInDays = {
                        'Ref': "LogRetentionInDays"
                    };
                }
            };
            // update IAM role follow new lambda function name
            if ('IamRoleLambdaExecution' in resources) {
                var statements = resources['IamRoleLambdaExecution']['Properties']['Policies'][0]['PolicyDocument']['Statement'];
                const deleted_statements = [];
                for (const index in statements) {
                    var first_action = statements[index]['Action'][0];
                    if (first_action.startsWith('logs')) {
                        for (const statementResource in statements[index]['Resource']) {
                            if ("Fn::Sub" in statements[index]['Resource'][statementResource]) {
                                let statementResourceValue = statements[index]['Resource'][statementResource]["Fn::Sub"];
                                if (statementResourceValue.endsWith(":*:*")) {
                                    statements[index]['Resource'][statementResource]["Fn::Sub"] = statementResourceValue.replace(":*:*", "-${Environment}:*:*")
                                }
                                else if (statementResourceValue.endsWith(":*")) {
                                    statements[index]['Resource'][statementResource]["Fn::Sub"] = statementResourceValue.replace(":*", "-${Environment}:*")
                                }
                            }
                        }
                    }
                }
                for (const index in deleted_statements.reverse()) {
                    statements.splice(deleted_statements[index], 1);
                }
            }
        }
    }
    modifyResourceS3Customs() {
        const template = this.serverless.service.provider.compiledCloudFormationTemplate;
        const {
            custom
        } = this.serverless.configurationInput;
        const resources = template.Resources;
        if (custom && 'onlyS3CustomResource' in custom) {
            const s3_custom_resources = [];
            const not_s3_custom_resources = [];
            const associated_s3_custom_resources = [];
            for (const item in resources) {
                if (resources[item]['Type'] == 'Custom::S3') {
                    s3_custom_resources.push(item);
                }
            }
            for (const index in s3_custom_resources) {
                const s3_custom_resource = s3_custom_resources[index];
                const lambda_s3_custom_resource = resources[s3_custom_resource]['Properties']['ServiceToken']['Fn::GetAtt'][0];
                resources[s3_custom_resource]['Properties']['ServiceToken'] = {
                    "Fn::Sub": "arn:aws:lambda:\${AWS::Region}:\${AWS::AccountId}:function:custom-resource-existing-s3-\${Environment}"
                }
                const functionName = resources[s3_custom_resource]['Properties']['FunctionName']
                resources[s3_custom_resource]['Properties']['FunctionName'] = {
                    "Fn::Sub": `${functionName}-\${Environment}`
                }
                const role_s3_custom_resource = resources[lambda_s3_custom_resource]['Properties']['Role']['Fn::GetAtt'][0];
                associated_s3_custom_resources.push(lambda_s3_custom_resource);
                associated_s3_custom_resources.push(role_s3_custom_resource);
                // force create only one s3 custom resource
                resources[lambda_s3_custom_resource]['Properties']['FunctionName'] = {
                    "Fn::Sub": 'custom-resource-existing-s3-\${Environment}'
                }
                // allow all resource for common s3 custom resource
                var statements = resources[role_s3_custom_resource]['Properties']['Policies'][0]['PolicyDocument']['Statement'];
                for (const index in statements) {
                    statements[index]['Resource'] = "*"
                    let awsService = statements[index]['Action'][0].split(':')[0];
                    statements[index]['Action'] = [`${awsService}:*`]
                }
            }
            if ('IamRoleCustomResourcesLambdaExecution' in resources) {
                var statements = resources['IamRoleCustomResourcesLambdaExecution']['Properties']['Policies'][0]['PolicyDocument']['Statement'];
                statements.push({
                    "Effect": "Allow",
                    "Resource": "*",
                    "Action": [
                        "logs:*"
                    ]
                });
            }

            // find all lambda function that is custom resource
            const lambda_functions = [];
            for (const item in resources) {
                if (associated_s3_custom_resources.indexOf(item) == -1) {
                    not_s3_custom_resources.push(item);
                    if (resources[item]['Type'] == 'AWS::Lambda::Function') {
                        lambda_functions.push(item);
                    }
                }
            }
            // Add dependencies for Custom::S3, make sure all Lambda functions are created before its
            for (const index in s3_custom_resources) {
                const s3_custom_resource = s3_custom_resources[index];
                resources[s3_custom_resource]['DependsOn'] = lambda_functions;
            }
            if (custom['onlyS3CustomResource']) {
                // remove all resource except Custom::S3
                for (const index in not_s3_custom_resources) {
                    delete resources[not_s3_custom_resources[index]]
                }
                resources['CustomResourceLogGroup'] = {};
                resources['CustomResourceLogGroup']['Type'] = 'AWS::Logs::LogGroup';
                resources['CustomResourceLogGroup']['Properties'] = {};
                resources['CustomResourceLogGroup']['Properties']['LogGroupName'] = 'custom-resource-existing-s3';
                resources['CustomResourceLogGroup']['Properties']['RetentionInDays'] = {
                    'Ref': 'LogRetentionInDays'
                };

            }
            else {
                // Delete all function related to Custom::S3
                for (const index in associated_s3_custom_resources) {
                    delete resources[associated_s3_custom_resources[index]]
                }
            }
        }

    }
    modifyResourceLambdas() {
        const template = this.serverless.service.provider.compiledCloudFormationTemplate;
        const parameters = template['Parameters'];
        parameters['S3ArtifactsPath'] = {
            "Type": "String",
            "Default": "serverless-artifact-3174403801224"
        };
        for (const resource in template.Resources) {
            const res = template.Resources[resource];
            if (res['Type'] === 'AWS::Lambda::Function') {
                // handle in case lambda use package zip
                if ('Code' in res['Properties']) {
                    var curS3Key = res['Properties']['Code']['S3Key'];
                    var packageName = curS3Key.substr(curS3Key.lastIndexOf('/') + 1)
                    res['Properties']['Code']['S3Key'] = {
                        'Fn::Sub': `\${S3ArtifactsPath}/${packageName}`
                    }
                    var functionName = res['Properties']['FunctionName'];
                    res['Properties']['FunctionName'] = {
                        'Fn::Sub': `${functionName}-\${Environment}`
                    }
                }
                else {
                    // handle in case lambda use container image
                }
                const {
                    custom
                } = this.serverless.configurationInput;
                if (custom && 'disableCloudWatchLog' in custom && custom['disableCloudWatchLog']) {
                    res['DependsOn'] = []
                }
            };
        }
    }
    modifyOutputs() {
        const template = this.serverless.service.provider.compiledCloudFormationTemplate
        if ('Outputs' in template && 'ServerlessDeploymentBucketName' in template['Outputs']) {
            delete template['Outputs']['ServerlessDeploymentBucketName'];
        }
    }

}


module.exports = serverlessLocalPlugin;