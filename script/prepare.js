#!/usr/bin/env node

const cp = require("child_process");

const requireOrInstall = async module => {
    try {
        require.resolve(module);
    } catch (e) {
        console.log(`Could not resolve "${module}"\nInstalling...`);
        cp.execSync(`npm install ${module}`);
        await new Promise(resolve => setTimeout(() => resolve(), 1000));
        await new Promise(resolve => setImmediate(() => resolve()));
        console.log(`"${module}" has been installed...`);
    }
    console.log(`Requiring "${module}"`);
    try {
        return require(module);
    } catch (e) {
        console.log(require.cache);
        console.log(e);
    }
}

function mergeObjects(objects, cb) {
    const r = {}
    objects.forEach(function (object) {
        Object.keys(object)
            .forEach(function (key) {
                const current = r[key]
                const next = object[key]
                if (current) {
                    if (typeof current === 'object' && typeof next === 'object') {
                        r[key] = mergeObjects([current, next], cb)
                    } else if (current !== next) {
                        r[key] = cb(current, next)
                    }
                } else {
                    r[key] = next
                }
            })
    })
    return r
}

const main = async () => {
    await requireOrInstall('yaml');
    const YAML = require('yaml');
    const fs = require('fs');

    function packageOverride() {
        const packageOrigin = JSON.parse(fs.readFileSync('./package.json', 'utf8'))
        try {
            if (fs.existsSync('./package.json')) {
                const merged = mergeObjects([packageOrigin, {
                    "dependencies": {
                        "bluebird": "^3.7.2",
                        "serverless": "^3.34.0",
                        "serverless-disable-aws-outputs": "^1.0.0"
                    }
                }], function (a, b) {
                    if (typeof a !== typeof b) {
                        console.error('Not sure how to merge', JSON.stringify(a), JSON.stringify(b))
                        process.exit(1)
                    } else {
                        return b
                    }
                })
                console.log(merged, "\n")
                fs.writeFileSync('./package.json', JSON.stringify(merged, null, 2), 'utf8');
            }
        } catch (err) {
            console.error(err)
        }
    }

    function serverlessLocalPlugin() {
        try {
            if (fs.existsSync('./serverless.yml')) {
                const serverlessOrigin = YAML.parse(fs.readFileSync('serverless.yml', 'utf8'))
                if ('plugins' in serverlessOrigin && serverlessOrigin['plugins'] != undefined) {
                    const pluginsAdd = serverlessOrigin['plugins'].push('./serverless-local-plugin/serverless-local-plugin.js')
                    console.log(YAML.stringify(serverlessOrigin))
                    fs.writeFileSync('./serverless.yml', YAML.stringify(serverlessOrigin), 'utf8');
                } else {
                    Object.assign(serverlessOrigin, {
                        "plugins": [
                            "./serverless-local-plugin/serverless-local-plugin.js"
                        ]
                    })
                    console.log(YAML.stringify(serverlessOrigin))
                    fs.writeFileSync('./serverless.yml', YAML.stringify(serverlessOrigin), 'utf8');
                }
            }
        } catch (err) {
            console.error(err)
        }
    }
    packageOverride()
    serverlessLocalPlugin()

}

main();