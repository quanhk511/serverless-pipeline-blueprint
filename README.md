# Shared GitLab serverless blueprint
Welcome to Shared pipeline blueprint for Serverless!    
This blueprint serves Serverless projects under BBX.  
## Import the Java Application Pipeline into a repository
The steps are the same as with Java Common Library Pipeline except for the ref in .gitlab-ci.yml the is different.
Example:   
```yaml
include:
  - project: gitlab-ci/shared-gitlab-blueprints/shared-gitlab-serverless-blueprint
    ref: main # checkout the repository to get the latest version https://gitlab.com/securitybankph/shared/bbx/devops/gitlab-ci/shared-gitlab-blueprints/shared-gitlab-serverless-blueprint/-/tags
    file: blueprint-template.yml
```